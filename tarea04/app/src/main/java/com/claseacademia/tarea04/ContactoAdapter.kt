package com.claseacademia.tarea04

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.claseacademia.tarea04.databinding.ItemContactosBinding

class ContactoAdapter (var contactos:List<Contactos> = listOf()) : RecyclerView.Adapter<ContactoAdapter.ViewHolder>(){

    //viewHolder
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        private val binding : ItemContactosBinding = ItemContactosBinding.bind(itemView)
        fun bind(contacto:Contactos){
          binding.tvName.text=contacto.name
          binding.tvCargo.text=contacto.cargo
          binding.tvEmail.text=contacto.email
         println("letra primer seria :"+ contacto.name)
            println("letra primer seria :"+ contacto.name.subSequence(0,1))
          binding.tvInicial.text=contacto.name.subSequence(0,1)
        }

    }

    fun updateList(contacto:List<Contactos>){
        this.contactos= contacto
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.item_contactos,parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val contacto = contactos[position]
        holder.bind(contacto)


    }

    override fun getItemCount(): Int {
        return contactos.size
    }
}