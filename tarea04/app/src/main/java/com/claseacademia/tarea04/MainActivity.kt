package com.claseacademia.tarea04

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.claseacademia.tarea04.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    //ViewBinding
     private lateinit var binding: ActivityMainBinding

    private lateinit var adapter :ContactoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        CargaAdapter()

        CargarData()

        events()
    }


    private fun CargaAdapter() {
       // rvContactos.adapter=//
        adapter = ContactoAdapter()
        binding.rvContactos.adapter= adapter

    }

    private fun CargarData() {
            }



    private fun events() {
        val contactos = listOf(
            Contactos(name ="Carlos 1" , cargo ="Cargo 1" , email ="email@1" ),
            Contactos(name ="Karlos 2" , cargo ="Cargo 2" , email ="email@2" ),
            Contactos(name ="Marlos 3" , cargo ="Cargo 3" , email ="email@3" ),
            Contactos(name ="Rarlos 4" , cargo ="Cargo 4" , email ="email@4" ),
            Contactos(name ="Tarlos 5" , cargo ="Cargo 5" , email ="email@5" ),
            Contactos(name ="Qarlos 6" , cargo ="Cargo 6" , email ="email@6" ),
            Contactos(name ="Oarlos 7" , cargo ="Cargo 7" , email ="email@7" ),
            Contactos(name ="Parlos 8" , cargo ="Cargo 8" , email ="email@8" ),
            Contactos(name ="Yarlos 9" , cargo ="Cargo 9" , email ="email@9" ),
            Contactos(name ="Zarlos 10" , cargo ="Cargo 10" , email ="email@10" ),
            Contactos(name ="Earlos 11" , cargo ="Cargo 11" , email ="email@11" ),
            Contactos(name ="Jarlos 11" , cargo ="Cargo 12" , email ="email@12" ),
            Contactos(name ="Sarlos 13" , cargo ="Cargo 13" , email ="email@13" ),
            )
        adapter.updateList(contactos)
    }
}