package com.claseacademia.lab02

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        //recibiendo Datos
        val bundle = intent.extras

        val name : String = bundle?.getString("KEY_NAME") ?:""
        val age : String = bundle?.getString("KEY_AGE") ?: ""
        var TipoMascota: String = bundle?.getString("KEY_TIPOMASCOTA") ?: ""
        val Adenovirus = bundle?.getString("KEY_ADENOVIRUS") ?:""
        val Rabia = bundle?.getString("KEY_RABIA") ?: ""
        val ParvoVirus = bundle?.getString("KEY_PARVOVIRUS") ?: ""
        val LeptosPirosis = bundle?.getString("KEY_LEPTOSPIROSIS") ?: ""
        val Distemper = bundle?.getString("KEY_DISTEMPER") ?: ""


        println("datos : $name $age $TipoMascota $Adenovirus $Rabia $ParvoVirus $LeptosPirosis $Distemper")

        val tvNames : TextView = findViewById(R.id.edtNameDetail)
        val tvAge : TextView = findViewById(R.id.tvAge)
        val mulTxt : EditText = findViewById(R.id.edtMulVacunas)
        tvNames.setText(name)
        tvAge.setText(age)
        mulTxt.setText(Adenovirus+" "+ Rabia +" "+ParvoVirus +" "+ LeptosPirosis +" "+ Distemper)

          if(TipoMascota=="Gato") {
              imgMascota.setImageResource(R.drawable.gato)
          } else if(TipoMascota=="Perro") {
              imgMascota.setImageResource(R.drawable.perro)
          } else if(TipoMascota=="Conejo") {
              imgMascota.setImageResource(R.drawable.conejo)
          }


    }
    }