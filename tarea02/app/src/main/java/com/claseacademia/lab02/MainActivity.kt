package com.claseacademia.lab02

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        val btnProcesar : Button = findViewById(R.id.btnProcesar)

        btnProcesar click {
            validaInfo()
        }
    }
        fun validaInfo() {

            //findViewById

            val edtName : EditText = findViewById(R.id.edtName)
            val edtAge : EditText = findViewById(R.id.edtEdad)
            val rbPerro: RadioButton = findViewById(R.id.rdbPerro)
            val rbGato : RadioButton = findViewById(R.id.rdbGato)
            val rdConejo : RadioButton = findViewById(R.id.rdbConejo)



            val chkAdenovirus : CheckBox = findViewById(R.id.chkAdenovirus)
            val chkRabia :CheckBox = findViewById(R.id.chkRabia)
            val chkDistemper :CheckBox = findViewById(R.id.chkDistemper)
            val chkParvovirus :CheckBox = findViewById(R.id.chkParvovirus)
            val chkLeptospirosis: CheckBox = findViewById(R.id.chkLeptospirosis)

            val name= edtName.text.toString()
            val age = edtAge.text.toString()

            // validando datos vacios

            if (name.isEmpty()) {
                ShowAlert(getString(R.string.valida_name))
                return
            }

            if (age.isEmpty()) {
                ShowAlert(getString(R.string.valid_age))
                return
            }


            //validando checkbok

            val chkAdenovirusTxt = if(chkAdenovirus.isChecked) chkAdenovirus.text.toString() else null
            val chkRabiaTxt = if(chkRabia.isChecked) chkRabia.text.toString() else null
            val chkDistemperTxt =if(chkDistemper.isChecked) chkDistemper.text.toString() else null
            val chkParvovirusTxt =if(chkParvovirus.isChecked) chkParvovirus.text.toString() else null
            val chkLeptospirosisTxt = if(chkLeptospirosis.isChecked) chkLeptospirosis.text.toString() else null

            // validando Radio Button
            val tipoMascota= rdbTipoMascota.checkedRadioButtonId

            val rdbutton : RadioButton = findViewById(tipoMascota)


            // create Bundle
            val bundle = Bundle().apply {
                putString("KEY_NAME",name)
                putString("KEY_AGE",age)
                putString("KEY_TIPOMASCOTA",rdbutton.text.toString())
                putString("KEY_ADENOVIRUS",chkAdenovirusTxt)
                putString("KEY_RABIA",chkRabiaTxt)
                putString("KEY_DISTEMPER",chkDistemperTxt)
                putString("KEY_PARVOVIRUS",chkParvovirusTxt)
                putString("KEY_LEPTOSPIROSIS",chkLeptospirosisTxt)
            }

            // pasando los valores por Intent
            val intent = Intent(this,DetailActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
