package com.claseacademia.lab02

import android.content.Context
import android.widget.Toast
import android.view.View

fun Context.ShowAlert(message:String) = Toast.makeText(this,message,Toast.LENGTH_LONG).show()

infix fun View.click(click:()->Unit){
    setOnClickListener { click()  }
}
